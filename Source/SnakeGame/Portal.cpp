// Fill out your copyright notice in the Description page of Project Settings.


#include "Portal.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
APortal::APortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PortalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PortalMesh"));
	PortalMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PortalMesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = PortalMesh;

	DeltaPortalVector = FVector(0.0f, 0.0f, 0.0f);
}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APortal::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			const FVector SnakeLocation = Snake->SnakeElements[0]->GetActorLocation();
			const FVector NewLocation = SnakeLocation + DeltaPortalVector;
			const FRotator SnakeRotation = Snake->SnakeElements[0]->GetActorRotation();
			
			Snake->SnakeElements[0]->SetActorTransform(FTransform(NewLocation));
			Snake->SnakeElements[0]->SetActorRotation(SnakeRotation);
		}
	}
}

