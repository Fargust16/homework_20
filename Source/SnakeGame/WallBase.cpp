// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "SnakeBase.h"
#include "Food.h"

// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WallMesh = CreateDefaultSubobject<UStaticMeshComponent>("WallMesh");
	WallMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallMesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = WallMesh;
}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();

	WallMesh->OnComponentBeginOverlap.AddDynamic(this, &AWallBase::BeginOverlap);
}

void AWallBase::Interact(AActor* Interactor, const bool bIsHead)
{
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

void AWallBase::BeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	const auto Food = Cast<AFood>(OtherActor);

	if (IsValid(Food))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("THE WALLLLLLL!!!!"));
		Food->ChangeFoodLocation();
	}
}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AWallBase::GetNewWallLocation(const float SnakeElemSize = 1, const float GroundHeight = 0, const float ArenaSize = 0)
{
	const FVector2D Location = GetLocation2D(SnakeElemSize, ArenaSize - SnakeElemSize);
	
	return FVector(Location.X, Location.Y, GroundHeight);
}

FVector2D AWallBase::GetLocation2D(const float SnakeElemSize, const float ArenaSize)
{
	FVector2D Location;

	do
	{
		Location = FVector2D(RandomFloat(SnakeElemSize, ArenaSize), RandomFloat(SnakeElemSize, ArenaSize));
	}
	while(abs(Location.X) <= SnakeElemSize * 3 && abs(Location.Y) <= SnakeElemSize * 3);
	
	return Location;
}

float AWallBase::RandomFloat(const float SnakeElemSize, const float ArenaSize)
{
	const int Max = static_cast<int>(ArenaSize / SnakeElemSize); 
	return SnakeElemSize * (-Max + rand() % (2 * Max + 1));
}

