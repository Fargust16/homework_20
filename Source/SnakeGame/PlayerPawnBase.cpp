// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Food.h"
#include "Portal.h"
#include "WallBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	PawnCamera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	PawnCamera->SetOrthoWidth(2300.0f);
	
	RootComponent = PawnCamera;
	ArenaSize = 600;
	PortalSize = 50;
	Score = 0;
	bIsPaused = false;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	SetActorTickInterval(2.0f);
	CreateSnakeActor();
	
	CreateWallActor(30);
	CreateFoodActor(1);
	CreatePortalActors();
}

// Called every frame
void APlayerPawnBase::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("BuyFood", IE_Pressed, this, &APlayerPawnBase::BuyFood);
	PlayerInputComponent->BindAction("BuyAbility", IE_Pressed, this, &APlayerPawnBase::BuyAbility);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->OnDestroyed.AddDynamic(this, &APlayerPawnBase::ShowEndScreen);
}

float APlayerPawnBase::GetSnakeElementSize() const
{
	return IsValid(SnakeActor) ? SnakeActor->ElementSize : 60;
}

float APlayerPawnBase::GetSnakeGroundHeight() const
{
	return IsValid(SnakeActor) ? SnakeActor->GroundHeight : 40;
}

void APlayerPawnBase::BuyFood()
{
	if (Score < 500) return;

	Score -= 500;
	CreateFoodActor(1);
}

void APlayerPawnBase::CreateFoodActor(int FoodsCount = 1)
{
	for (int i = 0; i < FoodsCount; ++i)
	{
		const float GroundHeight = GetSnakeGroundHeight();
		const float SnakeElemSize = GetSnakeElementSize();
		
		FTransform NewTransform(AFood::GetNewFoodLocation(SnakeElemSize, GroundHeight, 180));
	
		AFood* Food = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
		Food->SnakeActor = SnakeActor;
		Food->PawnActor = this;
	}
}

void APlayerPawnBase::CreateWallActor(int WallsCount) const
{
	for (int i = 0; i < WallsCount; ++i)
	{
		const float GroundHeight = GetSnakeGroundHeight();
		const float SnakeElemSize = GetSnakeElementSize();
		
		FTransform NewTransform(AWallBase::GetNewWallLocation(SnakeElemSize, GroundHeight, ArenaSize));
	
		GetWorld()->SpawnActor<AWallBase>(WallActorClass, NewTransform);
	}
}

void APlayerPawnBase::CreatePortalActors() const
{
	SpawnPortal(1, 0);
	SpawnPortal(-1, 0);
	SpawnPortal(0, 1);
	SpawnPortal(0, -1);
}

void APlayerPawnBase::SpawnPortal(const int XDir, const int YDir) const
{
	const float GroundHeight = GetSnakeGroundHeight();
	const float SnakeElemSize = GetSnakeElementSize();
	
	constexpr float PWidth = 12;
	constexpr float PHeight = 0.25;
	const float PDelta = PortalSize * PHeight;

	const float LocationX = XDir * (ArenaSize + PDelta);
	const float LocationY = YDir * (ArenaSize + PDelta);
	
	const float ScaleX = XDir != 0 ? PHeight : PWidth;
	const float ScaleY = YDir != 0 ? PHeight : PWidth;
	
	const float TeleportX = -XDir * (2 * ArenaSize) + XDir * SnakeElemSize;
	const float TeleportY = -YDir * (2 * ArenaSize) + YDir * SnakeElemSize;
	
	const FVector Location(LocationX, LocationY, GroundHeight);
	const FVector Scale(ScaleX, ScaleY, 0);
	const FVector TeleportTo(TeleportX, TeleportY, 0);
	
	APortal* Portal = GetWorld()->SpawnActor<APortal>(PortalActorClass, FTransform(Location));
	
	Portal->SetActorScale3D(Scale);
	Portal->DeltaPortalVector = TeleportTo;
}

void APlayerPawnBase::HandlePlayerVerticalInput(const float AxisValue)
{
	if (IsValid(SnakeActor) && SnakeActor->bCanChangeDirection) 
	{
		if (AxisValue > 0 && SnakeActor->LastMoveDirection != EMovementDirection::Down)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Up;
			SnakeActor->bCanChangeDirection = false;
			SnakeActor->SnakeElements[0]->SetActorRotation(FRotator(0.f, 180.f, 0.f));
		}
		else if (AxisValue < 0 && SnakeActor->LastMoveDirection != EMovementDirection::Up)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Down;
			SnakeActor->bCanChangeDirection = false;
			SnakeActor->SnakeElements[0]->SetActorRotation(FRotator(0.f, 0.f, 0.f));
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(const float AxisValue)
{
	if (IsValid(SnakeActor) && SnakeActor->bCanChangeDirection)
	{
		if (AxisValue > 0 && SnakeActor->LastMoveDirection != EMovementDirection::Left)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Right;
			SnakeActor->bCanChangeDirection = false;
			SnakeActor->SnakeElements[0]->SetActorRotation(FRotator(0.f, 90.f, 0.f));
		}
		else if (AxisValue < 0 && SnakeActor->LastMoveDirection != EMovementDirection::Right)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Left;
			SnakeActor->bCanChangeDirection = false;
			SnakeActor->SnakeElements[0]->SetActorRotation(FRotator(0.f, -90.f, 0.f));
		}
	}
}

int APlayerPawnBase::ShowScore()
{
	return Score;
}

void APlayerPawnBase::AddPoints()
{
	Score += 100;
}

void APlayerPawnBase::BuyAbility()
{
	if (Score < 1000) return;

	Score -= 1000;
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Ability works fine"));
}

void APlayerPawnBase::ShowEndScreen(AActor* DestroyedActor)
{
	bIsPaused = true;

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	AGameModeBase* MyGameMode = World->GetAuthGameMode();
	if (!ensure(MyGameMode != nullptr)) return;

	APlayerController* MyController = World->GetFirstPlayerController();
	if (!ensure(MyController != nullptr)) return;

	MyController->bShowMouseCursor = true; 
	MyController->bEnableClickEvents = true; 
	MyController->bEnableMouseOverEvents = true;

	MyGameMode->SetPause(MyController);
}

void APlayerPawnBase::NewGame()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	
	Score = 0;
	bIsPaused = false;
	UGameplayStatics::OpenLevel(World, "SnakeZone");
}

void APlayerPawnBase::Quit()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	
	APlayerController* MyController = World->GetFirstPlayerController();
	if (!ensure(MyController != nullptr)) return;

	MyController->ConsoleCommand("quit");
	
}

