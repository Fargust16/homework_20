// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 1.6f;
	MinMovementSpeed = 0.25f;
	GroundHeight = 40.f;
	LastMoveDirection = EMovementDirection::Down;
	bCanChangeDirection = true;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
}

void ASnakeBase::ChangeMoveSpeed()
{
	const float NewMoveSpeed = MovementSpeed / log(SnakeElements.Num() * 3);
	if (NewMoveSpeed > MinMovementSpeed)
	{
		SetActorTickInterval(NewMoveSpeed);
	}
}

// Called every frame
void ASnakeBase::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(const int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++) {
		FTransform NewTransform(GetLocationForNextElement());
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		
		//NewSnakeElement->MeshComponent->AttachToComponent(this->RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0) {
			NewSnakeElement->SetFirstElementType();
		}
	}
	
	SnakeElements.Last(1)->ResetElementType();
	SnakeElements.Last()->SetLastElementType();
	SnakeElements.Last()->SetActorRotation(SnakeElements.Last(1)->GetActorRotation());

	if (SnakeElements.Num())
	{
		ChangeMoveSpeed();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
		case EMovementDirection::Up: 
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::Down:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::Left:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::Right:
			MovementVector.Y -= ElementSize;
			break;
	}

	if (SnakeElements.Num())
	{
		SnakeElements[0]->ToggleCollision();

		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			const auto CurrentElement = SnakeElements[i];
			const auto PrevElement = SnakeElements[i - 1];
			
			FVector PrevLocation = PrevElement->GetActorLocation();
			const FVector CurrentLocation = CurrentElement->GetActorLocation();
			
			CurrentElement->SetActorLocation(PrevLocation);

			if (i == SnakeElements.Num() - 1)
			{
				const FRotator TaleRotator = GetTailRotation(CurrentLocation, PrevLocation, CurrentElement->GetActorRotation());
				CurrentElement->SetActorRotation(TaleRotator);
			}
		}

		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		SnakeElements[0]->ToggleCollision();
		bCanChangeDirection = true;
	}
}

FVector ASnakeBase::GetLocationForNextElement()
{
	FVector NewLocation;
		
	if (SnakeElements.Num())
	{
		const ASnakeElementBase* LastSnakeElement = SnakeElements.Last();
		const FVector LastLocation = LastSnakeElement->GetTargetLocation();
		NewLocation.Set(LastLocation.X, LastLocation.Y, GroundHeight);
	} else
	{
		NewLocation.Set(SnakeElements.Num() * ElementSize, 0, GroundHeight);
	}

	return NewLocation;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) 
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		const bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		
		/*GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString(Other->GetName())); // who
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Blue, FString(OverlappedElement->GetName())); // to whom*/
		
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

FRotator ASnakeBase::GetTailRotation(FVector TailLocation, FVector PrevLocation, FRotator DefaultRotator) const
{
	const FVector Difference = PrevLocation - TailLocation;

	if (Difference.X != 0 && abs(Difference.X) == ElementSize)
	{
		const float Rotation = Difference.X > 0 ? 180.0f : 0.0f;
		return FRotator(0.0f, Rotation, 0.0f);
	}

	if (Difference.Y != 0 && abs(Difference.Y) == ElementSize)
	{
		const float Rotation = Difference.Y > 0 ? -90.0f : 90.0f;
		return FRotator(0.0f, Rotation, 0.0f);
	}
	
	return DefaultRotator;
}

