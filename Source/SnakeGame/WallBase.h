// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "WallBase.generated.h"

UCLASS()
class SNAKEGAME_API AWallBase : public AActor, public  IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* WallMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	static FVector GetNewWallLocation(float SnakeElemSize, float GroundHeight, float ArenaSize);

	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult &SweepResult
	);

private:
	static FVector2D GetLocation2D(float SnakeElemSize, float ArenaSize);

	static float RandomFloat(float SnakeElemSize, float ArenaSize);
};
