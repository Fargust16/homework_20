// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class ASnakeBase;
class APlayerPawnBase;

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* FoodMesh;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	
	UPROPERTY(BlueprintReadWrite)
	APlayerPawnBase* PawnActor;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	static FVector GetNewFoodLocation(float SnakeElemSize, float GroundHeight, float MaxSpawnArea);

	UFUNCTION()
	void ChangeFoodLocation();

	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult &SweepResult
	);

private:
	static float RandomFloat(float SnakeElemSize, float ArenaSize);
};
