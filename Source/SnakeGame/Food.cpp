// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	FoodMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodMesh"));
	FoodMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FoodMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
	FoodMesh->OnComponentBeginOverlap.AddDynamic(this, &AFood::BeginOverlap);
}

void AFood::BeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	const auto Food = Cast<AFood>(OtherActor);
	
	if (IsValid(Food))
	{
		Food->ChangeFoodLocation();
	}
}


// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, const bool bIsHead)
{
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->ChangeFoodLocation();
			
			PawnActor->AddPoints();
		}
	}
}

void AFood::ChangeFoodLocation()
{
	if (!IsValid(SnakeActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Couldn't find SnakeActor"));
		return;
	}
	
	if (!IsValid(PawnActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Couldn't find PawnActor"));
		return;
	}
	
	const FVector NewLocation(
		GetNewFoodLocation(
			SnakeActor->ElementSize,
			SnakeActor->GroundHeight,
			PawnActor->ArenaSize
		)
	);
	
	this->SetActorLocation(
		NewLocation,
		false,
		nullptr,
		ETeleportType::ResetPhysics
	);
}

FVector AFood::GetNewFoodLocation(const float SnakeElemSize = 1.f, const float GroundHeight = 0.f, const float MaxSpawnArea = 0.f)
{
	const float X = RandomFloat(SnakeElemSize, MaxSpawnArea - SnakeElemSize);
	const float Y = RandomFloat(SnakeElemSize, MaxSpawnArea - SnakeElemSize);
	
	return FVector(X, Y, GroundHeight);
}

float AFood::RandomFloat(const float SnakeElemSize, const float ArenaSize)
{
	const int Max = static_cast<int>(ArenaSize / SnakeElemSize); 
	const int Result = -Max + rand() % (2 * Max + 1);
	
	return SnakeElemSize * Result;
}

