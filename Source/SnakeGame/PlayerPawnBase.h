// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class AWallBase;
class APortal;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;
	
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallBase> WallActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APortal> PortalActorClass;
	
	UPROPERTY(EditDefaultsOnly)
	float ArenaSize;

	UPROPERTY(EditDefaultsOnly)
	float PortalSize;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	int Score;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool bIsPaused;
	
private:
	FTimerHandle FoodTimerHandle;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	void CreateWallActor(int WallsCount) const;

	void CreatePortalActors() const;
	
	UFUNCTION()
	void BuyFood();
	
	UFUNCTION()
	void BuyAbility();
	
	UFUNCTION()
	void ShowEndScreen(AActor* DestroyedActor);
	
	UFUNCTION()
	void CreateFoodActor(int FoodsCount);

	UFUNCTION()
	void HandlePlayerVerticalInput(float AxisValue);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float AxisValue);

	UFUNCTION(BlueprintGetter)
	int ShowScore();

	UFUNCTION()
	void AddPoints();

	UFUNCTION(BlueprintCallable)
	void NewGame();

	UFUNCTION(BlueprintCallable)
	void Quit();

private:
	void SpawnPortal(int XDir, int YDir) const;
	
	float GetSnakeElementSize() const;
	
	float GetSnakeGroundHeight() const;
};
